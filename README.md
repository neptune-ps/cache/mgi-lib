# mgi-lib
A RuneScape cache library used to interact with the cache at a low level.
Originally made by mgi125 with some bug fixes and renaming.

## Installation
```groovy
repositories {
    maven { url = 'https://gitlab.com/api/v4/projects/14582104/packages/maven' }
}

dependencies {
    compile group: 'com.neptune', name: 'mgi-lib', version: '1.0'
}
```

## Introduction
This library is meant to be used for interacting with the cache at a low level,
this means that it does not come with the tools needed to encode specific parts
of the cache such as the different config types.

### Basic cache opening/closing
Opening and closing a cache reference is a matter of calling `Cache.openCache`
and `Cache#close` when done using the cache. Modifications made to the cache
are stored in memory until `Cache#close` or `Cache#flush` is called.
```java
Cache cache = Cache.openCache("./cache");
...
cache.close();
```

### Accessing a file
```java
// assuming Old School RuneScape
Archive configs = cache.getArchive(2);
Group npcs = configs.findGroupByID(9);
File abyssalDemon = npcs.findFileByID(415);
ByteBuffer data = abyssalDemon.getData();
```

### Creating/Deleteing a file
```java
// assuming Old School RuneScape
Archive configs = cache.getArchive(2);
Group npcs = configs.findGroupByID(9);
File abyssalDemon = npcs.findFileByID(415);

// delete file from cache
npcs.removeFile(abyssalDemon);

// add the file back (noop)
npcs.addFile(abyssalDemon);
```

## Contributing
Bug reports and pull requests are welcome on [GitLab](https://gitlab.com/neptune-ps/cache/mgi-lib).
